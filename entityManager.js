"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityManagerConfiguration = exports.EntityManager = void 0;
class EntityManager {
    constructor(configuration) {
        this._entityManagerConfiguration = configuration;
    }
    getById(keys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (keys == null || keys == undefined)
                throw new Error('Invalid Rut Error');
            else {
                try {
                    const filter = this.getPKFilter(this.entityManagerConfiguration.primaryKey, keys);
                    yield this.pool()
                        .query("SELECT * FROM " + this.entityManagerConfiguration.tableName + filter)
                        .then((res) => {
                        this._structure = res.rows[0];
                    })
                        .catch(() => { throw new Error("No record was found"); });
                }
                catch (e) {
                    throw new Error("Primary key incorrectly formed");
                }
            }
            return this._structure;
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.pool()
                    .query("SELECT * FROM " + this.entityManagerConfiguration.tableName)
                    .then((res) => {
                    this._structure = res.rows;
                })
                    .catch(() => { throw new Error("No record was found"); });
            }
            catch (e) {
                throw new Error("Primary key incorrectly formed");
            }
            return this._structure;
        });
    }
    getNextId() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.pool()
                    .query("SELECT MAX(ID) FROM " + this.entityManagerConfiguration.tableName)
                    .then((res) => {
                    this._structure = res.rows[0].max;
                })
                    .catch(() => { throw new Error("No record was found"); });
            }
            catch (e) {
                throw new Error("Primary key incorrectly formed");
            }
            return this._structure;
        });
    }
    delete(keys) {
        return __awaiter(this, void 0, void 0, function* () {
            if (keys == null || keys == undefined)
                throw new Error('Invalid id Error');
            else {
                try {
                    const filter = this.getPKFilter(this.entityManagerConfiguration.primaryKey, keys);
                    yield this.pool()
                        .query("DELETE FROM " + this.entityManagerConfiguration.tableName + filter)
                        .then((res) => {
                        this._structure = res.rowCount;
                    })
                        .catch(() => { throw new Error("No record deleted"); });
                }
                catch (e) {
                    throw new Error("Primary key incorrectly formed");
                }
            }
            return this._structure;
        });
    }
    save(record) {
        return __awaiter(this, void 0, void 0, function* () {
            if (record == null || record == undefined)
                throw new Error("Incorrect record cannot be saved");
            else {
                let values = "";
                let fields = "";
                let init = true;
                for (var property in record) {
                    if (init) {
                        fields = fields + property;
                        if (typeof record[property] === 'string') {
                            values = values + "'" + record[property] + "'";
                        }
                        if (typeof record[property] === 'number') {
                            values = values + record[property];
                        }
                        if (record[property] instanceof Date) {
                            values = values + "'" + this.formatDate(record[property]) + "'";
                        }
                    }
                    else {
                        fields = fields + "," + property;
                        if (typeof record[property] === 'string') {
                            values = values + ", '" + record[property] + "'";
                        }
                        if (typeof record[property] === 'number') {
                            values = values + ", " + record[property];
                        }
                        if (record[property] instanceof Date) {
                            values = values + ", '" + this.formatDate(record[property]) + "'";
                        }
                    }
                    init = false;
                }
                yield this.pool()
                    .query("INSERT INTO " + this.entityManagerConfiguration.tableName + "(" + fields + ") VALUES(" + values + ")")
                    .then((res) => {
                    this._structure = res.rowCount;
                })
                    .catch(() => { throw new Error("No record inserted"); });
            }
            return this._structure;
        });
    }
    getPKFilter(fields, keys) {
        var sentence = ' where ';
        var i = 0;
        fields.forEach((field) => {
            if (i > 0)
                sentence = sentence + " AND ";
            if (typeof keys[i] === 'string') {
                sentence = sentence + field + " = '" + keys[i] + "'";
            }
            if (typeof keys[i] === 'number') {
                sentence = sentence + field + " = " + keys[i];
            }
            if (keys[i] instanceof Date) {
                sentence = sentence + +field + " = '" + this.formatDate(keys[i]) + "'";
            }
            i++;
        });
        return sentence;
    }
    formatDate(date) {
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }
    pool() {
        const Pool = require('pg').Pool;
        return new Pool({
            host: this._entityManagerConfiguration.dbHost,
            port: this._entityManagerConfiguration.dbPort,
            user: this._entityManagerConfiguration.dbUser,
            password: this._entityManagerConfiguration.dbPassword,
            database: this._entityManagerConfiguration.dbDataBaseName
        });
    }
    get structure() {
        return this._structure;
    }
    set structure(value) {
        this._structure = value;
    }
    get entityManagerConfiguration() {
        return this._entityManagerConfiguration;
    }
    set entityManagerConfiguration(value) {
        this._entityManagerConfiguration = value;
    }
}
exports.EntityManager = EntityManager;
class EntityManagerConfiguration {
    constructor(structurePath, tableName, primaryKey, host, port, database, user, password) {
        this._structurePath = structurePath;
        this._dbTableName = tableName;
        this._primaryKey = primaryKey;
        this._dbHost = host;
        this._dbPort = port;
        this._dbDataBaseName = database;
        this._dbUser = user;
        this._dbPassword = password;
    }
    dataBaseConfiguration(host, port, database, user, password) {
        this._dbHost = host;
        this._dbPort = port;
        this._dbDataBaseName = database;
        this._dbUser = user;
        this._dbPassword = password;
    }
    get dbDataBaseName() {
        return this._dbDataBaseName;
    }
    get dbPassword() {
        return this._dbPassword;
    }
    get dbUser() {
        return this._dbUser;
    }
    get dbPort() {
        return this._dbPort;
    }
    get dbHost() {
        return this._dbHost;
    }
    get structurePath() {
        return this._structurePath;
    }
    get tableName() {
        return this._dbTableName;
    }
    get primaryKey() {
        return this._primaryKey;
    }
}
exports.EntityManagerConfiguration = EntityManagerConfiguration;
