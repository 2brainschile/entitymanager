
export class EntityManager {

    private _structure: any;
    private _entityManagerConfiguration: EntityManagerConfiguration;

    constructor(configuration: EntityManagerConfiguration) {
        this._entityManagerConfiguration = configuration;
    }


    public async getById(keys: Array<any>): Promise<any>{
        if(keys==null || keys==undefined) throw new Error('Invalid Rut Error');
		else{
			try{
				const filter = this.getPKFilter(this.entityManagerConfiguration.primaryKey, keys);
                await this.pool()
                .query("SELECT * FROM " + this.entityManagerConfiguration.tableName + filter)
                .then((res: { rows: any[]; }) => {
                    this._structure = res.rows[0];
                })
                .catch(() => { throw new Error("No record was found")})
			}catch(e){
				throw new Error("Primary key incorrectly formed");
			}
		} 
        return this._structure
    }

    public async getAll(): Promise<any>{
        try{
            await this.pool()
            .query("SELECT * FROM " + this.entityManagerConfiguration.tableName)
            .then((res: { rows: any[]; }) => {
                this._structure = res.rows;
            })
            .catch(() => { throw new Error("No record was found")})
        }catch(e){
            throw new Error("Primary key incorrectly formed");
        }
        return this._structure
    }

    public async getNextId(): Promise<any>{
        try{
            await this.pool()
            .query("SELECT MAX(ID) FROM " + this.entityManagerConfiguration.tableName)
            .then((res: any) => {
                this._structure = res.rows[0].max;
            })
            .catch(() => { throw new Error("No record was found")})
        }catch(e){
            throw new Error("Primary key incorrectly formed");
        }
        return this._structure
    }

    public async delete(keys: Array<any>): Promise<any>{
        if(keys==null || keys==undefined) throw new Error('Invalid id Error');
		else{
			try{
				const filter = this.getPKFilter(this.entityManagerConfiguration.primaryKey, keys);
                await this.pool()
                .query("DELETE FROM " + this.entityManagerConfiguration.tableName + filter)
                .then((res: any) => {
                    this._structure = res.rowCount;	
                })
                .catch(() => { throw new Error("No record deleted")})
			}catch(e){
				throw new Error("Primary key incorrectly formed");
			}
		} 
        return this._structure
    }

    public async save(record: any): Promise<any>{
		if(record==null || record==undefined) throw new Error("Incorrect record cannot be saved");
		else{
			let values = "";
			let fields = "";
			let init = true;
            for(var property in record){
                if(init){
					fields = fields + property;	
					if(typeof record[property] === 'string'){
						values = values + "'" + record[property] + "'";	
					}
					if(typeof record[property] === 'number'){
						values = values + record[property];	
					}
					if(record[property] instanceof Date){
						values = values + "'" + this.formatDate(record[property]) + "'";	
					}
				} 
                else{
					fields = fields + "," + property;	
					if(typeof record[property] === 'string'){
						values = values + ", '" + record[property] + "'";	
					}
					if(typeof record[property] === 'number'){
						values = values + ", " + record[property];	
					}
					if(record[property] instanceof Date){
						values = values + ", '" + this.formatDate(record[property]) + "'";	
					}
				}
				init = false;
            }
            await this.pool()
                .query("INSERT INTO " + this.entityManagerConfiguration.tableName  + "(" + fields + ") VALUES(" + values + ")")
                .then((res: any) => {
                    this._structure = res.rowCount;	
                })
                .catch(() => { throw new Error("No record inserted")})

        }

        return this._structure;
	}





    private getPKFilter(fields: Array<string>, keys: Array<any>){

		var sentence = ' where ';
		var i = 0;

		fields.forEach((field) =>{
			if(i>0) sentence = sentence + " AND ";
			if(typeof keys[i] === 'string'){
				sentence = sentence + field + " = '" + keys[i] + "'";
			}
			if(typeof keys[i] === 'number'){
				sentence = sentence + field + " = " + keys[i];	
			}
			if(keys[i] instanceof Date){
				sentence = sentence +  + field + " = '" + this.formatDate(keys[i]) + "'";
			}
			i++;
		});

		return sentence;
	}

    private formatDate(date: Date){
		return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	}

    public pool(){
        const Pool = require('pg').Pool;
        return new Pool({
            host: this._entityManagerConfiguration.dbHost,
            port: this._entityManagerConfiguration.dbPort,
            user: this._entityManagerConfiguration.dbUser,
            password: this._entityManagerConfiguration.dbPassword,
            database: this._entityManagerConfiguration.dbDataBaseName
        });

    }

    public get structure() {
        return this._structure;
    }

    public set structure(value: any) {
        this._structure = value;
    }

    public get entityManagerConfiguration() {
        return this._entityManagerConfiguration;
    }

    public set entityManagerConfiguration(value: any) {
        this._entityManagerConfiguration = value;
    }

}



export class EntityManagerConfiguration {

	private _structurePath: string;
	private _primaryKey: string[];
    private _dbHost: string;
    private _dbPort: number;
    private _dbUser: string;
    private _dbPassword: string;
    private _dbDataBaseName: string;
	private _dbTableName: string;

        constructor(structurePath: string, tableName: string, primaryKey: any[], host: string, port: number, database: string, user: string, password: string){
        this._structurePath = structurePath;
        this._dbTableName = tableName;
        this._primaryKey = primaryKey;
        this._dbHost = host;
        this._dbPort = port;
        this._dbDataBaseName = database;
        this._dbUser = user;
        this._dbPassword = password;
    }

    public dataBaseConfiguration(host: string, port: number, database: string, user: string, password: string){
        this._dbHost = host;
        this._dbPort = port;
        this._dbDataBaseName = database;
        this._dbUser = user;
        this._dbPassword = password;
    }

    public get dbDataBaseName() {
        return this._dbDataBaseName;
    }

    public get dbPassword() {
        return this._dbPassword;
    }

    public get dbUser() {
        return this._dbUser;
    }

    public get dbPort() {
        return this._dbPort;
    }
    
    public get dbHost() {
        return this._dbHost;
    }

    public get structurePath() {
        return this._structurePath;
    }

    public get tableName() {
        return this._dbTableName;
    }

    public get primaryKey() {
        return this._primaryKey;
    }
    
}