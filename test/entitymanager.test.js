const assert = require('assert')
const chai = require('chai');
const expect = chai.expect
const entityManager = require('../entityManager')

let configuration;
let manager;


describe('Get a client by id', () => {

    // Arrange
    before(() => {
        console.log('\tPreparing Entity Manager')
        configuration = new entityManager.EntityManagerConfiguration('/dataStructure/client.ts','clients', ['id'], 
        'localhost', 5432, 'Clients', 'postgres', 'Nolase01');
        manager = new entityManager.EntityManager(configuration);
    });

    it('Client succesfully returned', async () => {
        //Act
        const res = await manager.getById([14]);
        //Assert
        assert.equal(14, res.id);
    });

    it('Throws incorrect key error', async () => {
        // Act & Assert
        await assert.rejects(async () => await manager.getById([]), new Error('Primary key incorrectly formed'));
    });

    it('Client doesnt exist', async () => {
        //Act
        const res = await manager.getById([1]);
        //Assert
        assert.equal(undefined, res);
    });
  
    //Teardown
    after(() => {
        manager.pool().end();
        console.log('\tDatabase Connection closed');
    });
});

describe('Get all clients', () => {

    // Arrange
    before(() => {
        console.log('\tPreparing Entity Manager')
        configuration = new entityManager.EntityManagerConfiguration('/dataStructure/client.ts','clients', ['id'], 
        'localhost', 5432, 'Clients', 'postgres', 'Nolase01');
        manager = new entityManager.EntityManager(configuration);
    });

    it('Client list sucessfully returned', async () => {
        //Act
        const res = await manager.getAll();
        //Assert
        expect(res).to.have.lengthOf.above(0);
        expect(res[0].id).to.be.above(1);
    });

    //Teardown
    after(() => {
        manager.pool().end();
        console.log('\tDatabase Connection closed');
    });
});

describe('Insert new client', () => {

    // Arrange
    before(() => {
        console.log('\tPreparing Entity Manager')
        configuration = new entityManager.EntityManagerConfiguration('/dataStructure/client.ts','clients', ['id'], 
        'localhost', 5432, 'Clients', 'postgres', 'Nolase01');
        manager = new entityManager.EntityManager(configuration);
    });
    const structure = {
        rut: "14685119-3",
        firstname: "Obispo",
        city: "Santiago",
        region: "Metropolitana",
        address: "Calle La Paloma",
        email: "obi@hotmail.cl",
        name: "Jacinto"
    }

    it('Insert record', async () => {
        //Act
        const res = await manager.save(structure);
        //Assert
        assert.equal(1, res);
    });

    it('Insert record with mandatory nulls', async () => {
        //Act & Assert
        structure.rut = null;
        await assert.rejects(async () => await manager.save(structure), new Error('No record inserted'));
    });

    //Teardown
    after(() => {
        manager.pool().end();
        console.log('\tDatabase Connection closed');
    });
});

describe('delete a client', () => {

    // Arrange
    before(() => {
        console.log('\tPreparing Entity Manager')
        configuration = new entityManager.EntityManagerConfiguration('/dataStructure/client.ts','clients', ['id'], 
        'localhost', 5432, 'Clients', 'postgres', 'Nolase01');
        manager = new entityManager.EntityManager(configuration);
    });

    it('Client succesfully deleted', async () => {
        //Act
        const max = await manager.getNextId();
        const res = await manager.delete([max]);
        //Assert
        assert.equal(1, res);
    });

    it('Client delete Throws incorrect key error', async () => {
        // Act & Assert
        await assert.rejects(async () => await manager.delete([]), new Error('Primary key incorrectly formed'));
    });

    it('Client to delete doesnt exist', async () => {
        //Act
        const res = await manager.getById([1]);
        //Assert
        assert.equal(undefined, res);
    });
  
    //Teardown
    after(() => {
        manager.pool().end();
        console.log('\tDatabase Connection closed');
    });
});






